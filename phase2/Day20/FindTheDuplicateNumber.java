/*Find the Duplicate Number
Given an array of integers nums containing n + 1 integers where each integer is in the
range [1, n] inclusive.
There is only one repeated number in nums, return this repeated number.
You must solve the problem without modifying the array nums and uses only constant
extra space.
Example 1:
Input: nums = [1,3,4,2,2]
Output: 2
Example 2:
Input: nums = [3,1,3,4,2]
Output: 3

Constraints:
1 <= n <= 105
nums.length == n + 1
1 <= nums[i] <= n
All the integers in nums appear only once except for precisely one integer which appears
two or more times.
*/
import java.util.*;
class Solution {
	static int findDuplicate (int nums[]){
		 boolean[] tmp = new boolean[nums.length + 1];
		 int n = -1;
		 for (int i = 0; i < nums.length; i++) {
			 n = nums[i];
			 if (tmp[n]) {
				 return n;
			 }
			 tmp[n] = true;
		 }
		 return -1;
	}
	public static void main(String [] args){
		int nums[] = new int[]{1,3,4,2,2};
		System.out.println(Arrays.toString(nums));
		int ret = findDuplicate(nums);
		System.out.println(ret);
	}
}
