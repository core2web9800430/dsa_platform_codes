/* Find All Duplicates in an Array
 Given an integer array nums of length n where all the integers of nums are in the range
 [1, n] and each integer appears once or twice, return an array of all the integers that appears
 twice.
 You must write an algorithm that runs in O(n) time and uses only constant extra space.

 Example 1:
 Input: nums = [4,3,2,7,8,2,3,1]
 Output: [2,3]
 Example 2:
 Input: nums = [1,1,2]
 Output: [1]
 Example 3:
 Input: nums = [1]
 Output: []

 Constraints:
 n == nums.length
 1 <= n <= 105
 1 <= nums[i] <= n
 */

import java.util.*;
class Duplicates {
	 public List<Integer> findDuplicates(int[] nums) {
	 	 List<Integer> ln = new ArrayList<>();
		 HashSet<Integer> hs = new HashSet<>();
		 for(int i =0;i<nums.length;i++){
			 if(hs.contains(nums[i])){
				 ln.add(nums[i]);
			 }else {
				 hs.add(nums[i]);
			 }
		 }
		 return ln;
	 }
	 public static void main(String  [] args){
		 int nums[] = {4,3,2,7,8,2,3,1};
		 Duplicates obj = new Duplicates();
		 List<Integer> l = obj.findDuplicates(nums);
		 System.out.println(l);
	 }
}

