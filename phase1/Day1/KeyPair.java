/*Given an array Arr of N positive integers and another number X.
Determine whether or not there exist two elements in Arr whose sum is exactly X.
Example 1:
Input:
N = 6, X = 16
Arr[] = {1, 4, 45, 6, 10, 8}
Output: Yes
Explanation: Arr[3] + Arr[4] = 6 + 10 = 16

Example 2:
Input:
N = 5, X = 10
Arr[] = {1, 2, 4, 3, 6}
Output: Yes
Explanation: Arr[2] + Arr[4] = 4 + 6 = 10
Your Task: You don't need to read input or print anything. Your task is to complete the function 
hasArrayTwoCandidates() which takes the array of integers arr, n and x as parameters and returns 
a boolean denoting the answer.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 ≤ N ≤ 105
1 ≤ Arr[i] ≤ 105
*/
import java.util.*;
class KeyPair {
	boolean isContainPair(int arr [], int x){
		HashSet set = new HashSet();
		for(int i=0;i<arr.length;i++){
			int sec= x-arr[i];
			if(set.contains(sec)){
				return true;
			} else{
				set.add(arr[i]);
			}
		}
		return false;
	}
	public static void main(String [] args){
		int x=10;
		int arr[] = {1, 2, 4, 3, 6};
		KeyPair obj = new KeyPair();
		if( obj.isContainPair(arr,x))
		        System.out.println("Yes");
		else
			System.out.println("No"); 
	}
}


