/* Single Number
Company : Amazon, wipro, Capgemini, DXC technology, Schlumberger,
	Avizva, epam, cadence, paytm, atlassian,cultfit+7
	Platform: LeetCode - 136
	Striver’s SDE Sheet
	Description:
	Given a non-empty array of integers nums, every element appears
	twice except for one. Find that single one.
	You must implement a solution with a linear runtime complexity and use
	only constant extra space.
	Example 1:
	Input: nums = [2,2,1]
	Output: 1
	Example 2:
	Input: nums = [4,1,2,1,2]
	Output: 4
	Example 3:
	Input: nums = [1]
	Output: 1
	Constraints:
	1 <= nums.length <= 3 * 10^4
	-3 * 104 <= nums[i] <= 3 * 10^4
	Each element in the array appears twice except for one element
	which appears only once
*/
 import java.util.*;
 class SingleNumber {
	 int singleNumber(int [] arr){
		 for(int i=0;i<arr.length;i++){
			 int count=0;
			 for(int j=0;j<arr.length;j++){
				 if(arr[i]==arr[j]){
					 count++;
				 }
			 }
			 if(count==1)
				 return i;
		 }
		 return -1;
	 }
	 public static void main(String [] args){
		 int arr[]=new int[]{4,1,2,1,2};
		 SingleNumber obj = new SingleNumber();
		int ret = obj.singleNumber(arr);
		if(ret!=-1)
			System.out.println(arr[ret]);
		else
			System.out.println("No Single Element Present");
	 }
 }

