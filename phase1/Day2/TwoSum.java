/*Two Sum :
Given an array of integers nums and an integer target, 
return indices of the two numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you may not use the same element twice.
You can return the answer in any order.
Example 1:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].

Example 2:
Input: nums = [3,2,4], target = 6
Output: [1,2]

Example 3:
Input: nums = [3,3], target = 6
Output: [0,1]
*/

import java.util.*;
class TwoSum {
	int[] twoSum(int [] arr, int target){
		HashMap <Integer,Integer> hm = new HashMap<Integer,Integer>();
	        for(int i=0;i<arr.length;i++){
		int temp = target-arr[i];
		if(hm.containsKey(temp) && hm.get(temp)!=i){
			return new int[] {hm.get(temp),i};
		} else {
			hm.put(arr[i],i);
		   }
		}
		return new int[] {};
	}
	public static void main(String [] args){
	int arr[] = {2,7,11,15};
	int target  = 9;
	TwoSum obj = new TwoSum();
	int arr2[] = obj.twoSum(arr,target);
	for(int i:arr2){
		System.out.print(i+" ");
	}
	}
}	

 
