/*Remove Element
Given an integer array nums and an integer val, remove all occurrences of val in nums in-place. 
The order of the elements may be changed. Then return the number of elements in nums which are not equal to val.
Consider the number of elements in nums which are not equal to val be k, to get accepted, 
you need to do the following things:
Change the array nums such that the first k elements of nums contain the elements which are not equal to val.
The remaining elements of nums are not important as well as the size of nums. Return k;
Example 1:
Input: nums = [3,2,2,3], val = 3
Output: 2, nums = [2,2,_,_]
Explanation: Your function should return k = 2, with the first two elements of nums being 2.
It does not matter what you leave beyond the returned k (hence they are underscores).

Example 2:
Input: nums = [0,1,2,2,3,0,4,2], val = 2
Output: 5, nums = [0,1,4,0,3,_,_,_]
Explanation: Your function should return k = 5, with the first five elements of 
nums containing 0, 0, 1, 3, and 4.
Note that the five elements can be returned in any order.
It does not matter what you leave beyond the returned k (hence they are underscores).
Constraints:
0 <= nums.length <= 100
0 <= nums[i] <= 50
0 <= val <= 100
*/
import java.util.*;
class RemoveElement {
         static int removeElement(int arr[],int val){
		int k = 0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]!=val){
				arr[k]=arr[i];
				k++;
			}
		}
		return k;
	}
	public static void main(String [] args){
		int[] nums = {3,2,2,3}; 
		int val = 3; 
		int[] expectedNums = {2,2}; 
	       	int k = removeElement(nums, val); 	
		for(int i:nums){
			System.out.print(i+" ");
		}
	       	assert k == expectedNums.length;
	       	Arrays.sort(nums, 0, k); 
	        for (int i = 0; i <nums.length; i++) {
	        assert nums[i] == expectedNums[i];
	    	}
		System.out.println("k = "+k);
		for(int i:nums){
			System.out.print(i+" ");
		}
	}
}

