/* Code :Even Odd
Given an array arr[] of size N containing equal number of odd and even numbers. Arrange the numbers in such 
a way that all the even numbers get the even index and odd numbers get the odd index.
Note: There are multiple possible solutions, Print any one of them. Also, 0-based indexing is considered.
Example 1:
Input:
N = 6
arr[] = {3, 6, 12, 1, 5, 8}
Output:
1
Explanation:
6 3 12 1 8 5 is a possible solution. The output will always be 1 if your rearrangement is correct.

Example 2:
Input:
N = 4
arr[] = {1, 2, 3, 4}
Output :
1
Your Task:  You don't need to read input or print anything. Your task is to complete the function reArrange() which takes an integer N and an array arr of size N as input and reArranges the array in Place without any extra space.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 105
1 ≤ arr[i] ≤ 105
*/
 
import java.util.*;
class EvenOdd {
	static void reArrange(int[] arr, int N) {
		int even = 0;
		int odd =1;
		/*int arr2 [] = new int [N];
		for(int i = 0;i<arr.length;i++){
			if(arr[i]%2==0){
				arr2[even]=arr[i];
				even+=2;
			}else {
				arr2[odd]=arr[i];
				odd+=2;
			}
		}						        
		for(int i=0;i<N;i++){
			arr[i]=arr2[i];
		}*/
		while(even<N && odd<N){
			if(arr[even]%2!=0 && arr[odd]%2==0){
				int temp = arr[even];
				arr[even]=arr[odd];
				arr[odd] = temp;
			}
			if(arr[even]%2==0)
				even+=2;
			if(arr[odd]%2!=0)
				odd+=2;
		}

	}
	public static void main(String [] args){
		int arr[] = {3, 6, 12, 1, 5, 8};
		EvenOdd obj = new EvenOdd();
		obj.reArrange(arr,arr.length);
		for(int i:arr){
			System.out.print(i+" ");
		}
	}
}

