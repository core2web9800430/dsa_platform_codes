/*Pascal's Triangle
Given an integer numRows, return the first numRows of Pascal's triangle.
In Pascal's triangle, each number is the sum of the two numbers directly above it as shown:

Example 1:
Input: numRows = 5
Output: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]

Example 2:
Input: numRows = 1
Output: [[1]]

Constraints:
    1 <= numRows <= 30
 */
import java.util.*;
class PascalsTriangle {
      List<List<Integer>> pascalsTriangle(int row) {
		if(row<=0)
			return new ArrayList();
		List<List<Integer>> result = new ArrayList();
		for(int i=0; i<row;i++){
			List<Integer> newal = new ArrayList();
			for(int j =0;j<=i;j++){
				if(j==0 || j==i){
					newal.add(1);
				} else {
					newal.add(result.get(i-1).get(j)+result.get(i-1).get(j-1));
				}
			}
			result.add(newal);
		}
		return result;
	}
	public static void main(String [] args){
		int row = 5;
		 PascalsTriangle obj = new  PascalsTriangle();
		 List<List<Integer>> l = obj. pascalsTriangle(row);
		 System.out.println(l);
	}
}

