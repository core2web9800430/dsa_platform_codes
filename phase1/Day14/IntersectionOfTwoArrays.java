/*Intersection of Two Arrays
Given two integer arrays nums1 and nums2, return an array of their intersection. 
Each element in the result must be unique and you may return the result in any order.

Example 1:
Input: nums1 = [1,2,2,1], nums2 = [2,2]
Output: [2]

Example 2:
Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
Output: [9,4]
Explanation: [4,9] is also accepted.

Constraints:
1 <= nums1.length, nums2.length <= 1000
0 <= nums1[i], nums2[i] <= 1000
*/

import java.util.*;
class Solution {
	int[] intersection(int[] nums1, int[] nums2) {
		HashSet<Integer> hs1 = new HashSet<>();
		HashSet<Integer> hs2 = new HashSet<>();
		for(int i =0;i<nums1.length; i++){
			hs1.add(nums1[i]);
		}
		for(int i=0; i<nums2.length;i++){
			if(hs1.contains(nums2[i]))
					hs2.add(nums2[i]);
		}
		int arr [] = new int[hs2.size()];
		int i=0;
		for(int n : hs2){
			arr[i] = n;
			i++;
		}
		return arr;
	}
	public static void main(String[] args){
		//int nums1 [] = new int[]{1,2,2,1};
		//int nums2 [] = new int[]{2,2};
		int nums1 [] = new int[]{4,9,5};
		int nums2 [] = new int[]{9,4,9,8,4};
		Solution obj = new Solution();
		int arr[] = obj.intersection(nums1,nums2);
		System.out.println(Arrays.toString(arr));
	}
}



