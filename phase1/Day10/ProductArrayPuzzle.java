/*Product array puzzle
Given an array nums[] of size n, construct a Product Array P (of same size n) such that P[i] is equal to the product of all the elements of nums except nums[i].
Example 1:
Input:
n = 5
nums[] = {10, 3, 5, 6, 2}
Output:
180 600 360 300 900
Explanation: 
For i=0, P[i] = 3*5*6*2 = 180.
For i=1, P[i] = 10*5*6*2 = 600.
For i=2, P[i] = 10*3*6*2 = 360.
For i=3, P[i] = 10*3*5*2 = 300.
For i=4, P[i] = 10*3*5*6 = 900.

Example 2:
Input:
n = 2
nums[] = {12,0}
Output:
0 12
Your Task:
You do not have to read input. Your task is to complete the function productExceptSelf() that takes array nums[] and n as input parameters and returns a list of n integers denoting the product array P. If the array has only one element the returned list should should contains one value i.e {1}
Note: Try to solve this problem without using the division operation.

Expected Time Complexity: O(n)
Expected Auxiliary Space: O(n)
*/

 import java.util.*;
 class ArrayPuzzle {
	  static long[] productExceptSelf(int nums[], int n)  { 
		  long [] arr = new long[n];
		  int k = 0;
		  while(k<n){
			  long pro =1;
			  for(int i=0; i<n; i++){
				  if(i!=k){
					  pro*=nums[i];
				  }
			  }
			  arr[k]=pro;
			  k++;
		  }
		  return arr;
	  } 
	 public static void main(String [] args){
		 int nums[] = {10, 3, 5, 6, 2};
		 for(int i:nums)
			 System.out.print(i+" ");
		 System.out.println("\n Product Array is:180 600 360 300 900");
		 long [] arr = productExceptSelf(nums,nums.length);
		 for(long i:arr)
			 System.out.print(i+" ");
		 System.out.println();
	 }
 }



