/*Bitonic Point
Given an array arr of n elements that is first strictly increasing and then maybe strictly decreasing, find the maximum element in the array.
Note: If the array is increasing then just print the last element will be the maximum value.

Example 1:
Input: 
n = 9
arr[] = {1,15,25,45,42,21,17,12,11}
Output: 45
Explanation: Maximum element is 45.

Example 2:
Input: 
n = 5
arr[] = {1, 45, 47, 50, 5}
Output: 50
Explanation: Maximum element is 50.

Your Task:  
You don't need to read input or print anything. Your task is to complete the function findMaximum() 
which takes the array arr[], and n as parameters and returns an integer denoting the answer.

Expected Time Complexity: O(logn)
Expected Auxiliary Space: O(1)

Constraints:
3 ≤ n ≤ 106
1 ≤ arri ≤ 106
*/
public class FindMaximumElement {
	public static int findMaximum(int[] arr, int n) {
		if (n == 1) {
			return arr[0];
		}
		int left = 0, right = n - 1;
		while (left < right) {
			int mid = left + (right - left) / 2;
			if (arr[mid] > arr[mid + 1]) {
				right = mid;
			} else {
				left = mid + 1;
			}
		}
		return arr[left];
	}
	public static void main(String[] args) {
		int[] arr1 = {1, 15, 25, 45, 42, 21, 17, 12, 11};
		int n1 = arr1.length;
		System.out.println("Maximum element: " + findMaximum(arr1, n1));
		int[] arr2 = {1, 45, 47, 50, 5};
		int n2 = arr2.length;
		System.out.println("Maximum element: " + findMaximum(arr2, n2));
	}
}
