/*Maximum Product of Three Numbers
Given an integer array nums, find three numbers whose product is maximum and return the maximum product.
Example 1:
Input: nums = [1,2,3]
Output: 6

Example 2:
Input: nums = [1,2,3,4]
Output: 24

Example 3:
Input: nums = [-1,-2,-3]
Output: -6
Constraints:
3 <= nums.length <= 104
-1000 <= nums[i] <= 1000
*/

class MaximumProductOfThreeNumbers {
	int maximumProduct(int[] arr){
		 int max1 = Integer.MIN_VALUE;
		 int max2 = Integer.MIN_VALUE;
		 int max3 = Integer.MIN_VALUE;
		 int min1 = Integer.MAX_VALUE;
		 int min2 = Integer.MAX_VALUE;
		 for(int i=0;i<arr.length;i++){
			 if(arr[i]>max3){
				if(arr[i]>max2){
					if(arr[i]>max1){
				 max3 = max2;
				 max2 = max1;
				 max1 = arr[i];
					} else{
					max3 = max2;
					max2=arr[i];
					}
				} else
				    max3=arr[i];
			 }
			 if(arr[i]<min2){
				 if(arr[i]<min1){
					 min2=min1;
					 min1=arr[i];
				 } else
				 min2=arr[i];
			 }
		 }
		 return Math.max(max1*max2*max3,min1*min2*min1);
	}	
	public static void main(String[] args){
	//int arr[] = {1,2,3};
	int arr[] = {1,2,3,4};
	 MaximumProductOfThreeNumbers obj = new  MaximumProductOfThreeNumbers ();
	 int ret = obj.maximumProduct(arr);
	 System.out.println("Maximum Product is: "+ret);
	}
}

