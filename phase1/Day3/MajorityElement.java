/*Majority Element
Given an array nums of size n, return the majority element.
The majority element is the element that appears more than ⌊n / 2⌋ times. 
You may assume that the majority element always exists in the array.

Example 1:
Input: nums = [3,2,3]
Output: 3

Example 2:
Input: nums = [2,2,1,1,1,2,2]
Output: 2
Constraints:
n == nums.length
1 <= n <= 5 * 104
-109 <= nums[i] <= 109
*/
import java.util.*;
class MajorityElement {
	public int majorityElement(int[] nums) {
		HashMap<Integer,Integer> hm = new HashMap<Integer, Integer>();
		for(int i=0;i<nums.length;i++){
			if(hm.containsKey(nums[i]))
					hm.put(nums[i],hm.get(nums[i])+1);
			else
				hm.put(nums[i],1);
		}
		for (Map.Entry<Integer, Integer> e : hm.entrySet()) {
			            if (e.getValue() > nums.length/2) {
					   return e.getKey();
				    }
		}
		return 0;

	}
	public static void main(String [] args){
		//int nums[] ={2,2,1,1,1,2,2};
		int nums[] = {3,2,3};
		MajorityElement obj = new MajorityElement();
		int ret = obj.majorityElement(nums);
		System.out.println(ret);
	}
}
